#include <LiquidCrystal.h>
// define pin numbers
#define RS 7
#define ENABLE 6
#define D4 5
#define D5 4
#define D6 3
#define D7 2

// create LCD
LiquidCrystal lcd(RS, ENABLE, D4, D5, D6, D7);

const int led = 13;
int eta;
int a;
int ret;

void setup() {
  // initialize LCD (use 16 colums and 2 lines)
  lcd.begin(16, 2);
  ////
  Serial.begin(9600);
  pinMode(led, OUTPUT);
}

void loop() {
  if (Serial.available() > 0) {
    a = Serial.read();
    if (a == 1){
      Serial.write(1);
      digitalWrite(led, HIGH);
      //Text
      lcd.print("Etat de led:");
      lcd.setCursor(0, 1);
      lcd.print("ON              ");
    }
    else {
      Serial.write(0);
      digitalWrite(led, LOW);
      //Text
      lcd.print("Etat de led:");
      lcd.setCursor(0, 1);
      lcd.print("OFF              ");
    }
  }
}

// © Maxence Brochier 2018 - 2019