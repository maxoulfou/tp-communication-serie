# TP communication série

- [x] Codes sources Arduino (.ino)
- [x] Fichiers sources schéma électrique (Proteus)
---
- [x] Codes Arduino commentés

<img src = "https://gitlab.com/maxoulfou/tp-communication-serie/raw/master/Images/communication_s%C3%A9rie.PNG" title = "schéma" alt = "Communication série Schema">

© Maxence Brochier 2018 - 2019