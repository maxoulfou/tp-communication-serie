#include <LiquidCrystal.h>
// define pin numbers
#define RS 7
#define ENABLE 6
#define D4 5
#define D5 4
#define D6 3
#define D7 2

// create LCD
LiquidCrystal lcd(RS, ENABLE, D4, D5, D6, D7);

const int button = 13;
const int ledret = 12;
int eta;
int ret;

void setup() {
  // initialize LCD (use 16 colums and 2 lines)
  lcd.begin(16, 2);
  ////
  Serial.begin(9600);
  pinMode(button, INPUT);
  pinMode(ledret, OUTPUT);
}

void loop() {
  eta = digitalRead(button);
  if (eta == 1) {
    Serial.write(1);
    //Text
      lcd.print("Etat de button:");
      lcd.setCursor(0, 1);
      lcd.print("Appuye           ");
  } else {
    Serial.write(0);
    //Text
      lcd.print("Etat de button:");
      lcd.setCursor(0, 1);
      lcd.print("Non appuye       ");
  }
  ///////////////////////////////////
  if (Serial.available() > 0) {
    ret = Serial.read();
      if (ret == 1) {
        delay(500);
        digitalWrite(ledret, HIGH); 
      } else {
        delay(10);
        digitalWrite(ledret, LOW);
      }
  }
}

// © Maxence Brochier 2018 - 2019